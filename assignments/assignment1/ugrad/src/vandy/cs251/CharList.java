package vandy.cs251;

import java.lang.IndexOutOfBoundsException;

/**
 * Provides a wrapper facade around primitive char lists, allowing
 * for dynamic resizing.
 */
public class CharList 
             implements Comparable<CharList>, 
                        Cloneable {
    /**
     * The head of the list.
     */
    Node mHeadNode;

    /**
     * The current size of the list.
     */
    int mArraySize;

    /**
     * Default value for elements in the list.
     */
    char mDefaultVal;

    /**
     * Constructs an list of the given size.
     *
     * @param size Non-negative integer size of the desired list.
     */
    public CharList(int size) {
        this(size, '\0');
    }

    /**
     * Constructs an list of the given size, filled with the provided
     * default value.
     *
     * @param size Nonnegative integer size of the desired list.
     * @param defaultValue A default value for the list.
     * @throw IndexOutOfBoundsException If size < 0.
     */
    public CharList(int size, char defaultValue) {
        if(size<0){
            throw new IndexOutOfBoundsException();
        }
        else{
            mDefaultVal = defaultValue;
            mHeadNode = new Node();
            mHeadNode.nChar= mDefaultVal;
            Node prevNode = mHeadNode;
            for(int i = 0; i<size; i++){
                prevNode = new Node(mDefaultVal, prevNode);
            }
            mArraySize = size;
        }
    }

    /**
     * Copy constructor; creates a deep copy of the provided CharList.
     *
     * @param s The CharList to be copied.
     */
    public CharList(CharList s) {
        if(s.mHeadNode != null){
            mHeadNode = new Node();
            mHeadNode.nChar = s.mHeadNode.nChar;
            if(s.mHeadNode.next!= null){
                Node prevNode = mHeadNode;
                Node nextNode;
                Node iter = s.mHeadNode.next;
                while(iter!= null){
                    nextNode = new Node(iter.nChar, prevNode);
                    prevNode = nextNode;
                    iter = iter.next;
                }
            }
            mArraySize = s.mArraySize;
            mDefaultVal = s.mDefaultVal;
        }
    }

    /**
     * Creates a deep copy of this CharList.  Implements the
     * Prototype pattern.
     */
    @Override
    public Object clone() {
        if(this.mHeadNode != null){
            CharList temp = new CharList(this.size(), this.mDefaultVal);
            temp.mHeadNode = new Node();
            temp.mHeadNode.nChar = this.mHeadNode.nChar;
            if(this.mHeadNode.next!= null){
                Node prevNode = temp.mHeadNode;
                Node nextNode;
                Node iter = this.mHeadNode.next;
                while(iter!= null){
                    nextNode = new Node(iter.nChar, prevNode);
                    prevNode = nextNode;
                    iter = iter.next;
                }
            }
            return temp;
        }
        else{
            return new CharList(this.size(), this.mDefaultVal);
        }
    }

    /**
     * @return The current size of the list.
     */
    public int size() {
        return mArraySize;
    }

    /**
     * Resizes the list to the requested size.
     *
     * Changes the capacity of this list to hold the requested number of elements.
     * Note the following optimizations/implementation details:
     * <ul>
     *   <li> If the requests size is smaller than the current maximum capacity, new memory
     *   is not allocated.
     *   <li> If the list was constructed with a default value, it is used to populate
     *   uninitialized fields in the list.
     * </ul>
     * @param size Nonnegative requested new size.
     */
    public void resize(int size) {
        if(mArraySize<size){
            Node curNode = mHeadNode;
            while(curNode.next != null){
                curNode = curNode.next;
            }
            Node newNode = curNode;
            for(int i = mArraySize; i<size; i++){
                newNode = new Node(mDefaultVal, newNode);
                curNode = curNode.next;
                newNode = curNode;
            }
            mArraySize = size;
        }
        else if(mArraySize>size) {
            Node curNode = mHeadNode;
            for(int i = 0; i<size; i++){
                curNode = curNode.next;
            }
            curNode.prune();
            mArraySize = size;
        }
    }

    /**
     * @return the element at the requested index.
     * @param index Nonnegative index of the requested element.
     * @throws IndexOutOfBoundsException If the requested index is outside the
     * current bounds of the list.
     */
    public char get(int index) {
        return seek(index).nChar;
    }

    /**
     * Sets the element at the requested index with a provided value.
     * @param index Nonnegative index of the requested element.
     * @param value A provided value.
     * @throws IndexOutOfBoundsException If the requested index is outside the
     * current bounds of the list.
     */
    public void set(int index, char value) {
        seek(index).nChar = value;
    }

    /**
     * Locate and return the @a Node at the @a index location.
     */
    private Node seek(int index) {
        rangeCheck(index);
        Node curNode = mHeadNode;
        for(int i = 0; i<index; i++){
            curNode = curNode.next;
        }
        return curNode;
    }

    /**
     * Compares this list with another list.
     * <p>
     * This is a requirement of the Comparable interface.  It is used to provide
     * an ordering for CharList elements.
     * @return a negative value if the provided list is "greater than" this list,
     * zero if the lists are identical, and a positive value if the
     * provided list is "less than" this list. These lists should be compred
     * lexicographically.
     */
    @Override
    public int compareTo(CharList s) {
        int returnVariable = 0;
        int curIndex = 0;
        while(returnVariable == 0 && curIndex < Math.min(mArraySize, s.mArraySize)){
            returnVariable = (int) seek(curIndex).nChar - (int) s.seek(curIndex).nChar;
            curIndex++;
        }
        if(returnVariable==0) {
            if (s.size() > this.size()) {
                return -1;
            }
            else if(s.size() < this.size()) {
                return 1;
            }
            else{
                return 0;
            }
        }
        else{
            return returnVariable;
        }
    }

    /**
     * Throws an exception if the index is out of bound.
     */
    private void rangeCheck(int index) {
        if(mArraySize<index + 1 || index<0){
            throw new IndexOutOfBoundsException();
        }
    }

    /**
     * A Node in the Linked List.
     */
    private class Node {
        /**
         * Value stored in the Node.
         */
	    char nChar;

        /**
         * Reference to the next node in the list.
         */
	    Node next;

        /**
         * Default constructor (no op).
         */
        Node() {
        }

        /**
         * Construct a Node from a @a prev Node.
         */
        Node(Node prev) {
            prev.next = this;
        }

        /**
         * Construct a Node from a @a value and a @a prev Node.
         */
        Node(char value, Node prev) {
            prev.next = this;
            nChar = value;
        }

        /**
         * Ensure all subsequent nodes are properly deallocated.
         */
        void prune() {
            next = null;
        }
    }
}
