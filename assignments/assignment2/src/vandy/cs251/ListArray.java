package vandy.cs251;

import java.lang.ArrayIndexOutOfBoundsException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Provides a generic dynamically-(re)sized array abstraction.
 */
public class ListArray<T extends Comparable<T>>
             implements Comparable<ListArray<T>>,
                        Iterable<T> {
    /**
     * The underlying list of type T.
     */

    Node mHeadNode;

    /**
     * The current size of the array.
     */
    int mArraySize;

    /**
     * Default value for elements in the array.
     */
     T mDefaultVal = null;

    /**
     * Constructs an array of the given size.
     * @param size Nonnegative integer size of the desired array.
     * @throws NegativeArraySizeException if the specified size is
     *         negative.
     */
    @SuppressWarnings("unchecked")
    public ListArray(int size) throws NegativeArraySizeException {
        this(size, null);
    }

    /**
     * Constructs an array of the given size, filled with the provided
     * default value.
     * @param size Nonnegative integer size of the desired array.
     * @param defaultValue A default value for the array.
     * @throws NegativeArraySizeException if the specified size is
     *         negative.
     */
    public ListArray(int size,
                     T defaultValue) throws NegativeArraySizeException {
        if(size<0){
            throw new NegativeArraySizeException();
        }
        else{
            mDefaultVal = defaultValue;
            mHeadNode = new Node();
            mHeadNode.data = mDefaultVal;
            Node prevNode = mHeadNode;
            for(int i = 0; i<size; i++){
                prevNode = new Node(mDefaultVal, prevNode);
            }
            mArraySize = size;
        }
    }

    /**
     * Copy constructor; creates a deep copy of the provided array.
     * @param s The array to be copied.
     */
    public ListArray(ListArray<T> s) {

        // NJL - This is too complex. You only need 1 temp Node and 1 iterator.
        // -2 pts; Inefficient copy ctor
        if(s.mHeadNode != null){
            mHeadNode = new Node();
            mHeadNode.data = s.mHeadNode.data;
            if(s.mHeadNode.next!= null){
                Node prevNode = mHeadNode;
                Node nextNode;
                Node iter = s.mHeadNode.next;
                ListIterator curIter = new ListIterator();
                while(iter!= null){
                    nextNode = new Node(iter.data, prevNode);
                    prevNode = nextNode;
                    iter = iter.next;
                }
            }
            mArraySize = s.mArraySize;
            mDefaultVal = s.mDefaultVal;
        }
    }

    /**
     * @return The current size of the array.
     */
    public int size()
    {
        return mArraySize;
    }

    /**
     * Resizes the array to the requested size.
     *
     * Changes the size of this ListArray to hold the requested number of elements.
     * @param size Nonnegative requested new size.
     */
    public void resize(int size) {
        if(mArraySize<size){
            // NJL - Use the iterator Factory to create iterators. This is a Frequently Made Mistake.
            // -3 pts; iterator factory
            ListIterator curNode = new ListIterator();
            // NJL - You can simplify this using seek().
            // -2 pts; Inefficient resize
            while(curNode.hasNext()){
                curNode.next();
            }
            Node newNode = curNode.current();
            for(int i = mArraySize; i<size; i++){
                newNode = new Node(mDefaultVal, newNode);
                curNode.next();
                newNode = curNode.current();
            }
            mArraySize = size;
        }
        else if(mArraySize>size) {
            // NJL - Use the iterator Factory to create iterators. This is a Frequently Made Mistake.
            NodeIterator curNode = new NodeIterator(mHeadNode);
            // NJL - You can simplify this using seek().
            for(int i = 0; i<size; i++){
                curNode.next();
            }
            (curNode.current()).prune();
            mArraySize = size;
        }
    }

    /**
     * @return the element at the requested index.
     * @param index Nonnegative index of the requested element.
     * @throws ArrayIndexOutOfBoundsException If the requested index is outside the
     * current bounds of the array.
     */
    public T get(int index) {
        Node returnNode = this.seek(index);
        return returnNode.data;
    }

    /**
     * Sets the element at the requested index with a provided value.
     * @param index Nonnegative index of the requested element.
     * @param value A provided value.
     * @throws ArrayIndexOutOfBoundsException If the requested index is outside the
     * current bounds of the array.
     */
    public void set(int index, T value) {
        this.seek(index).data = value;
    }

    private Node seek(int index) {
        rangeCheck(index);
        // NJL - Use the iterator Factory to create iterators. This is a Frequently Made Mistake.
        NodeIterator curNode = new NodeIterator(mHeadNode);
        // NJL - The head Node doesn't count as part of the list, so you should start at your iterator.next.
        // -0 pts; Off-by-One seek
        // Update: This is OK for a non-dummy node implementation.
        for(int i = 0; i<index; i++){
            curNode.next();
        }
        return curNode.current();
    }

    /**
     * Removes the element at the specified position in this ListArray.
     * Shifts any subsequent elements to the left (subtracts one from their
     * indices).  Returns the element that was removed from the ListArray.
     *
     * @param index the index of the element to remove
     * @return element that was removed
     * @throws ArrayIndexOutOfBoundsException if the index is out of range.
     */
    public T remove(int index) {
        rangeCheck(index);
        if(index == 0){
            // NJL - You should never be able to remove the head. Index 0 means the first non-head Node.
            // -0 pts; Error removing head
            // Update: This is OK for a non dummy node implementation.
            Node removeNode = mHeadNode;
            mHeadNode = mHeadNode.next;
            removeNode.next = null;
            return removeNode.data;
        }
        else{
            // NJL - You should not use seek twice. Just find one node and access its .next.
            // -3 pts; Redundant seek
            Node prevNode = this.seek(index-1);
            Node removeNode = this.seek(index);
            prevNode.next = removeNode.next;
            removeNode.next = null;
            return removeNode.data;
        }
    }

    /**
     * Compares this array with another array.
     * <p>
     * This is a requirement of the Comparable interface.  It is used to provide
     * an ordering for ListArray elements.
     * @return a negative value if the provided array is "greater than" this array,
     * zero if the arrays are identical, and a positive value if the
     * provided array is "less than" this array.
     */
    @Override
    public int compareTo(ListArray<T> s) {
        int returnVariable = 0;
        int curIndex = 0;
        while(returnVariable == 0 && curIndex < Math.min(mArraySize, s.mArraySize)){
            // NJL - using get() is very inefficient here. This is a frequently made mistake.
            // -4 pts; using get() in CompareTo
            returnVariable = this.get(curIndex).compareTo(s.get(curIndex));
            curIndex++;
        }
        // NJL - This is very inefficient. Just return size - s.size.
        //  -2 pts
        if(returnVariable==0) {
            if (s.size() > this.size()) {
                return -1;
            }
            else if(s.size() < this.size()) {
                return 1;
            }
            else{
                return 0;
            }
        }
        else{
            return returnVariable;
        }

    }

    /** 
     * Throws an exception if the index is out of bound. 
     */
    private void rangeCheck(int index) {
        if(mArraySize<index + 1 || index<0){
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    /**
     * Factory method that returns an Iterator.
     */
    public Iterator<T> iterator() {
        return new ListIterator();
    }

    private class Node implements Iterable<Node> {
        T data;
        Node next;
        /**
         * Default constructor (no op).
         */
        Node() {
        }

        /**
         * Construct a Node from a @a prev Node.
         */
        Node(Node prev) {
            // NJL - you should check that prev != null before you try to access it.
            // -0 pts; Unchecked null
            // Update: This case should never occur if you handle your nodes correctly. Points refunded.

            // NJL - This is not correct. You have to set this.next to prev.next
            // -3 pts; Incorrect Node Constructor
            prev.next = this;
            data = null;
            next = null;
        }

        /**
         * Construct a Node from a @a value and a @a prev Node.
         */
        Node(T value, Node prev) {
           data = value;
            // NJL - You should implement this in terms of the other constructor.
            // -3 pts; Constructor re-use.
            prev.next = this;
            next = null;
        }

        /**
         * Ensure all subsequent nodes are properly deallocated.
         */
        void prune() {
            // NJL - You are missing a lot of things here.
            // -8 pts; No prune implementation
            next = null;
        }

        @Override
        public Iterator<Node> iterator() {
            NodeIterator iterator = new NodeIterator(this);
            return iterator;
        }
    }

    private class NodeIterator implements Iterator<Node> {
        Node nodeIterCurNode;
        boolean removeIsReady;
        Node prevNode;

        NodeIterator(Node node){
            nodeIterCurNode = node;
            removeIsReady = false;
        }

        public T data(){
            return nodeIterCurNode.data;
        }

        public Node current(){
            return nodeIterCurNode;
        }


        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return nodeIterCurNode.next != null;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iteration has no more elements
         */
        @Override
        public Node next() {
            if(!hasNext()){
                throw new NoSuchElementException();
            }
            else{
                prevNode = nodeIterCurNode;
                nodeIterCurNode = nodeIterCurNode.next;
                removeIsReady = true;
                return nodeIterCurNode;
            }
        }

        /**
         * Removes from the underlying collection the last element returned
         * by this iterator (optional operation).  This method can be called
         * only once per call to {@link #next}.  The behavior of an iterator
         * is unspecified if the underlying collection is modified while the
         * iteration is in progress in any way other than by calling this
         * method.
         *
         * @throws UnsupportedOperationException if the {@code remove}
         *                                       operation is not supported by this iterator
         * @throws IllegalStateException         if the {@code next} method has not
         *                                       yet been called, or the {@code remove} method has already
         *                                       been called after the last call to the {@code next}
         *                                       method
         * @implSpec The default implementation throws an instance of
         * {@link UnsupportedOperationException} and performs no other action.
         */
        @Override
        public void remove() {
            // NJL - This is too complicated. Use advantage of the fact that you constructed a reference to the node
            // you want to remove.
            // -3pts ; Inefficient remove
            if(prevNode != null){
                NodeIterator travNode = new NodeIterator(mHeadNode);
                if(travNode.nodeIterCurNode == prevNode){
                    Node removeNode = mHeadNode;
                    mHeadNode = mHeadNode.next;
                    removeNode.next = null;
                }
                else {
                    for (NodeIterator node = travNode; node.nodeIterCurNode != prevNode && node.nodeIterCurNode.next != prevNode; node.next()) {
                        //nothing to do
                    }
                    travNode.nodeIterCurNode.next = prevNode.next;
                    prevNode.next = null;
                }
                mArraySize--;
                prevNode = null;
            }
            else{
                throw new IllegalStateException();
            }
        }
    }

    /**
     * @brief This class implements an iterator for the list.
     */
    private class ListIterator implements Iterator<T> {
        NodeIterator listIterCurNode;

        ListIterator(){
            listIterCurNode = new NodeIterator(mHeadNode);
        }

        public Node current(){
            return listIterCurNode.current();
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iteration has no more elements
         */
        @Override
        public T next() {
                Node returnData = listIterCurNode.nodeIterCurNode;
            listIterCurNode.next();
                return returnData.data;
        }

        /**
         * Removes from the underlying collection the last element returned
         * by this iterator (optional operation).  This method can be called
         * only once per call to {@link #next}.  The behavior of an iterator
         * is unspecified if the underlying collection is modified while the
         * iteration is in progress in any way other than by calling this
         * method.
         *
         * @throws UnsupportedOperationException if the {@code remove}
         *                                       operation is not supported by this iterator
         * @throws IllegalStateException         if the {@code next} method has not
         *                                       yet been called, or the {@code remove} method has already
         *                                       been called after the last call to the {@code next}
         *                                       method
         * @implSpec The default implementation throws an instance of
         * {@link UnsupportedOperationException} and performs no other action.
         */
        @Override
        public void remove() {
            listIterCurNode.remove();
        }

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return listIterCurNode.hasNext();
        }
    }
}
